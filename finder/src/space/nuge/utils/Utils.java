package space.nuge.utils;

import space.nuge.setting.Setting;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static BufferedImage resize(BufferedImage image, int width, int height) {

        BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        newImage.getGraphics().drawImage(image, 0, 0, width, height, null);

        return newImage;
    }

    public static int[][] levelParser() throws Exception {

        Integer[][] result = null;
        int[][] res;

        BufferedReader reader = new BufferedReader(new FileReader(Setting.file));

        String line = null;
        List<Integer[]> lvlLines = new ArrayList<Integer[]>();

        while ((line = reader.readLine()) != null) {
            lvlLines.add(str2int_arrays(line.split(" ")));
        }
        result = new Integer[lvlLines.size()][lvlLines.get(0).length];
        for (int i = 0; i < lvlLines.size(); i++) {
            result[i] = lvlLines.get(i);
        }

        res = new int[result.length][result[0].length];

        for (int i = 0; i < res.length; i++) {
            for (int j = 0; j < res[0].length; j++) {
                res[i][j] = result[i][j];
            }
        }

        return res;
    }

    public static int[][] levelParser(String filePath) {

        Integer[][] result = null;
        int[][] res;
        try {
            InputStream in = Utils.class.getResourceAsStream(filePath);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line = null;
            List<Integer[]> lvlLines = new ArrayList<Integer[]>();
            while ((line = reader.readLine()) != null) {
                lvlLines.add(str2int_arrays(line.split(" ")));
            }
            result = new Integer[lvlLines.size()][lvlLines.get(0).length];
            for (int i = 0; i < lvlLines.size(); i++) {
                result[i] = lvlLines.get(i);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        res = new int[result.length][result[0].length];

        for (int i = 0; i < res.length; i++) {
            for (int j = 0; j < res[0].length; j++) {
                res[i][j] = result[i][j];
            }
        }

        return res;
    }

    public static final Integer[] str2int_arrays(String[] sArr) {
        Integer[] result = new Integer[sArr.length];
        for (int i = 0; i < sArr.length; i++) {
            result[i] = Integer.parseInt(sArr[i]);
        }
        return result;
    }
}
