package space.nuge.setting;

import java.io.File;

public abstract class Setting {
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;
    public static final int CLEAR_COLOR = 0xff000000;
    public static final int NUM_BUFFERS = 3;
    public static int MAX_ROW = 50;
    public static int MIN_ROW = 50;
    public static int MAX_COL = 10;
    public static int MIN_COL = 10;
    public static float PLAYER_SPEED = 2;

    public static int MAP_ROW;
    public static int MAP_COL;
    public static float TILE_WIDTH;
    public static float TILE_HEIGHT;

    public static File file = null;

    public static Boolean newClick = false;
    public static int Xpos;
    public static int Ypos;

}
