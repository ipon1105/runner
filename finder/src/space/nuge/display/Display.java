package space.nuge.display;

import space.nuge.game.Game;
import space.nuge.setting.Setting;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Arrays;

public abstract class Display{

    private static JFrame window;
    private static Canvas content;
    private static Game G;
    private static BufferedImage buffer;
    private static int[] bufferData;
    private static Graphics bufferGraphics;
    private static BufferStrategy bufferStrategy;

    public static void Start(){
        createDisplay();
    }

    public static void createDisplay(){
        window = new JFrame("Сыщик");

        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setPreferredSize(new Dimension(900, 900));

        final Font F = new Font("TimesRoman",Font.BOLD, 22);
        final JButton btnStart = new JButton("Старт");
        final JButton btnSetMap = new JButton("Установить карту");
        final JPanel mainPanel = new JPanel();

        btnStart.setFont(F);
        btnSetMap.setFont(F);

        btnStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                G = new Game();

                createGame();

                G.start();
            }
        });
        btnSetMap.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileopen = new JFileChooser();
                int ret = fileopen.showDialog(null, "Открыть файл");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    Setting.file = fileopen.getSelectedFile();
                }
            }
        });

        mainPanel.setLayout(new GridBagLayout());

        GridBagConstraints grid = new GridBagConstraints();
        grid.gridwidth = GridBagConstraints.REMAINDER;
        grid.fill = GridBagConstraints.HORIZONTAL;

        mainPanel.add(btnStart,grid);
        mainPanel.add(btnSetMap,grid);

        window.add(mainPanel);

        window.pack();
        window.setVisible(true);
    }


    public static void createGame(){
        window.getContentPane().removeAll();

        final JButton btn = new JButton("Применить");
        final JLabel txtSpeed = new JLabel("Скорость ");
        final JTextArea textArea = new JTextArea(1,5);
        final JPanel helpPanel = new JPanel();
        final JPanel downPanel = new JPanel();
        final JPanel mainPanel = new JPanel();

        mainPanel.setLayout(new BorderLayout());
        downPanel.setLayout(new FlowLayout());
        helpPanel.setLayout(new GridBagLayout());

        content = new Canvas();
        content.setPreferredSize(new Dimension(Setting.WIDTH, Setting.HEIGHT));
        content.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Setting.newClick = true;

                Setting.Xpos=(int)(e.getX()/Setting.TILE_WIDTH);
                Setting.Ypos=(int)(e.getY()/Setting.TILE_HEIGHT);
            }
        });
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (textArea.getText().equals(null)||textArea.getText().equals("")) return;
                try{
                    Setting.PLAYER_SPEED = Float.parseFloat(textArea.getText());
                }catch (Exception em){

                }
            }
        });

        GridBagConstraints grid = new GridBagConstraints();
        grid.gridwidth = GridBagConstraints.REMAINDER;
        grid.fill = GridBagConstraints.HORIZONTAL;

        helpPanel.add(content, grid);
        downPanel.add(txtSpeed);
        downPanel.add(textArea);
        downPanel.add(btn);
        mainPanel.add(helpPanel, BorderLayout.CENTER);
        mainPanel.add(downPanel, BorderLayout.SOUTH);


        window.add(mainPanel);

        contentService();

        window.pack();
        window.repaint();
        clear();
    }

    public static void contentService() {

        buffer = new BufferedImage(Setting.WIDTH, Setting.HEIGHT,BufferedImage.TYPE_INT_ARGB);
        bufferData = ((DataBufferInt) buffer.getRaster().getDataBuffer()).getData();
        bufferGraphics = buffer.getGraphics();
        ((Graphics2D)(bufferGraphics)).setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);

        content.createBufferStrategy(Setting.NUM_BUFFERS);

        bufferStrategy = content.getBufferStrategy();
    }

    public static void clear(){
        Arrays.fill(bufferData, Setting.CLEAR_COLOR);
    }

    public static void swapBuffers(){
        Graphics g = bufferStrategy.getDrawGraphics();
        g.drawImage(buffer,0,0,null);
        bufferStrategy.show();
    }

    public static Graphics2D getGraphics(){
        return (Graphics2D)bufferGraphics;
    }

}
