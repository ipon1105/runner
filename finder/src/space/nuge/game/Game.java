package space.nuge.game;

import space.nuge.display.Display;
import space.nuge.game.levels.FieldMap;
import space.nuge.game.player.Player;
import space.nuge.graphics.TextureAtlas;
import space.nuge.utils.Time;

import java.awt.*;

public class Game implements Runnable{

    public static final float UPDATE_RATE = 60.0f;
    public static final float UPDATE_INTERVAL = Time.SECOND / UPDATE_RATE;
    public static final long IDLE_TIME = 1;

    private Boolean running;
    private Thread gameThread;
    private Graphics2D graphics;
    private FieldMap fieldMap;

    public Game(){
        running = false;
        gameThread = new Thread(this);

        fieldMap = new FieldMap(new TextureAtlas("resource.png"));
    }

    public void update(){
        fieldMap.update();
    }

    public void render(){
        Display.clear();

        fieldMap.render(graphics);

        Display.swapBuffers();
    }

    public synchronized void start(){
        if (running) return;

        graphics = Display.getGraphics();

        running = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public synchronized void stop(){
        if (!running) return;
        running = false;
        try {
            gameThread.join(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void run() {

        long count = 0;
        float delta = 0;

        long lastTime = Time.get();

        while (running) {
            long now = Time.get();
            long elapsedTime = now - lastTime;
            lastTime = now;

            count+=elapsedTime;

            Boolean render = false;
            delta += (elapsedTime / UPDATE_INTERVAL);

            while (delta > 1) {
                update();
                delta--;

                if (!render) render = true;
            }

            if (render) render(); else {
                try {
                    Thread.sleep(IDLE_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (count>=Time.SECOND) count = 0;
        }
    }

}
