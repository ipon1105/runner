package space.nuge.game.player;

import space.nuge.setting.Setting;

import java.awt.*;

public class Player {
    private int X;
    private int Y;

    public Player(int X, int Y){
        this.X = X;
        this.Y = Y;
    }

    public void render(Graphics2D g) {
        g.setColor(Color.RED);
        g.fillOval((int) (X*Setting.TILE_WIDTH),(int) (Y*Setting.TILE_HEIGHT), (int)Setting.TILE_WIDTH,(int)Setting.TILE_HEIGHT);
    }

    public void Move(int way){

        switch (way){
            case 0://Вверх
                if (Y!=0) Y--;
                break;
            case 1://Вправо
                if (X!=Setting.MAP_COL) X++;
                break;
            case 2://Вниз
                if (Y!=Setting.MAP_ROW) Y++;
                break;
            case 3://Влево
                if (X!=0) X--;
                break;
            default: //Ничего не изменяем
        }

    }

    public int getX() {
        return X;
    }

    public int getY() {
        return Y;
    }
}
