package space.nuge.game.logistic;

import space.nuge.setting.Setting;

public class Logic {

    private int nowNum;
    private Boolean run;

    public int[] FindPath(int[][] map) {
        run = true;

        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                if (map[i][j] == -3) Summer(map, i, j, 1);
            }
        }

        nowNum = 1;
        while (run) {

            for (int i = 0; i < map.length; i++) {
                for (int j = 0; j < map[0].length; j++) {
                    if (map[i][j] == nowNum) Summer(map, i, j, nowNum + 1);
                }
            }

            nowNum++;

            if (nowNum > Setting.MAP_COL * Setting.MAP_ROW + 1) run = false;
        }

        run = true;
        return findPath(map);
    }

    private int[] findPath(int[][] map) {
        int[] path;
        int now = Setting.MAP_COL * Setting.MAP_ROW + 1;

        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                if (map[i][j] == -2)
                    now = FindLittle(i, j, now, map);
            }
        }
        path = new int[1];
        if (now == Setting.MAP_COL * Setting.MAP_ROW + 1) run = false;
        else path = new int[now+1];

        int a = 0;
        while (run) {

            for (int i = 0; i < map.length; i++) {
                for (int j = 0; j < map[0].length; j++) {
                    if (map[i][j] == -2) {
                        if (now == 0) {
                            path[a] = FindPyt(i, j, -3, map);
                        } else {
                            path[a] = FindPyt(i, j, now, map);
                        }
                        a++;
                        now--;
                        switch (path[a - 1]) {
                            case 0:
                                map[i - 1][j] = -2;
                                break;
                            case 1:
                                map[i][j + 1] = -2;
                                break;
                            case 2:
                                map[i + 1][j] = -2;
                                break;
                            case 3:
                                map[i][j - 1] = -2;
                                break;
                            default:
                        }

                        map[i][j] = 0;
                        i = map.length - 1;
                    }
                }
            }

            if (now==-1) run = false;
        }

        run = false;
        nowNum = 0;
        return path;
    }

    //Найти самый маленький в 4 сторонах
    private int FindLittle(int i, int j, int max, int[][] map) {

        if (i != 0)
            if (map[i - 1][j] < max && map[i - 1][j] > 0) max = map[i - 1][j];

        if (i < Setting.MAP_ROW - 1)
            if (map[i + 1][j] < max && map[i + 1][j] > 0) max = map[i + 1][j];

        if (j != 0)
            if (map[i][j - 1] < max && map[i][j - 1] > 0) max = map[i][j - 1];

        if (j < Setting.MAP_COL - 1)
            if (map[i][j + 1] < max && map[i][j + 1] > 0) max = map[i][j + 1];

        return max;
    }

    //Найти Сторону куда надо идти
    //0 - верх
    //1 - вправо
    //2 - вниз
    //3 - влево
    private int FindPyt(int i, int j, int max, int[][] map) {
        int res = -1;

        if (i != 0)
            if (map[i - 1][j] == max) res = 0;

        if (i < Setting.MAP_ROW - 1)
            if (map[i + 1][j] == max) res = 2;

        if (j != 0)
            if (map[i][j - 1] == max) res = 3;

        if (j < Setting.MAP_COL - 1)
            if (map[i][j + 1] == max) res = 1;

        return res;
    }

    private void Summer(int[][] map, int i, int j, int nextNum) {

        if (i != 0)
            if (map[i - 1][j] == -2) {
                run = false;
            } else if (map[i - 1][j] != -1 && map[i - 1][j] == 0) map[i - 1][j] = nextNum;

        if (i < Setting.MAP_ROW - 1)
            if (map[i + 1][j] == -2) {
                run = false;
            } else if (map[i + 1][j] != -1 && map[i + 1][j] == 0) map[i + 1][j] = nextNum;

        if (j != 0)
            if (map[i][j - 1] == -2) {
                run = false;
            } else if (map[i][j - 1] != -1 && map[i][j - 1] == 0) map[i][j - 1] = nextNum;

        if (j < Setting.MAP_COL - 1)
            if (map[i][j + 1] == -2) {
                run = false;
            } else if (map[i][j + 1] != -1 && map[i][j + 1] == 0) map[i][j + 1] = nextNum;
    }


/*
    public void ConsoleOut(int[][] map) {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                if (map[i][j] == -1) {
                    map[i][j] = 0;
                    System.out.print(map[i][j] + " ");
                    map[i][j] = -1;
                } else
                    System.out.print(map[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println();
        System.out.println();
    }
*/
}
