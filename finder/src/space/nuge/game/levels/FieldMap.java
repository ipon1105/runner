package space.nuge.game.levels;

import space.nuge.game.logistic.Logic;
import space.nuge.game.player.Player;
import space.nuge.graphics.TextureAtlas;
import space.nuge.setting.Setting;
import space.nuge.utils.Utils;

import java.awt.*;
import java.awt.image.BufferedImage;

public class FieldMap {
    private int[][] numericMap;
    private GameObjects[][] objectMap;

    private BufferedImage floor;
    private BufferedImage wall;

    private Player player;

    private float timer = 0;
    private int lang = 0;
    private int[] mapLong;

    public FieldMap(TextureAtlas atlas) {
        if (Setting.file != null) {
            try {
                numericMap = Utils.levelParser();
                if (numericMap.length < Setting.MIN_ROW || numericMap.length > Setting.MAX_ROW || numericMap[0].length < Setting.MIN_COL || numericMap[0].length > Setting.MAX_COL)
                    createMapStandert();
            } catch (Exception e) {
                createMapStandert();
            }
        } else {
            createMapStandert();
        }

        Setting.MAP_ROW = numericMap.length;
        Setting.MAP_COL = numericMap[0].length;

        objectMap = new GameObjects[numericMap.length][numericMap[0].length];

        Setting.TILE_WIDTH = Setting.WIDTH / numericMap[0].length;
        Setting.TILE_HEIGHT = Setting.HEIGHT / numericMap.length;

        ImageResize(atlas);
        setFields();
    }

    public void createMapStandert() {
        numericMap = Utils.levelParser("/resource/Test.lvl");
    }

    private void ImageResize(TextureAtlas atlas) {
        BufferedImage a = atlas.cut(0, 0, 83, 83);
        BufferedImage b = atlas.cut(2 * 84, 3 * 84, 84, 84);

        this.floor = Utils.resize(a, (int) (Setting.TILE_WIDTH), (int) (Setting.TILE_HEIGHT));
        this.wall = Utils.resize(b, (int) (Setting.TILE_WIDTH), (int) (Setting.TILE_HEIGHT));
    }

    private void setFields() {

        //0 - пол
        //-1 - стена
        //-2 - тот кто ходит
        //-3 - то куда ходят

        for (int i = 0; i < objectMap.length; i++) {
            for (int j = 0; j < objectMap[0].length; j++) {
                switch (numericMap[i][j]) {
                    case -1:
                        objectMap[i][j] = new GameObjects(j * Setting.TILE_WIDTH, i * Setting.TILE_HEIGHT, wall);
                        break;
                    case -2:
                        //Здесь будет тот кто ходит
                        objectMap[i][j] = new GameObjects(j * Setting.TILE_WIDTH, i * Setting.TILE_HEIGHT, floor);
                        player = new Player(j, i);
                        break;
                    case -3:
                        objectMap[i][j] = new GameObjects(j * Setting.TILE_WIDTH, i * Setting.TILE_HEIGHT, floor);
                        //Тут то куда надо идти
                        break;
                    default:
                        objectMap[i][j] = new GameObjects(j * Setting.TILE_WIDTH, i * Setting.TILE_HEIGHT, floor);
                        break;
                }
            }
        }
    }


    public void update() {
        if (mapLong != null) {
            if (lang < mapLong.length) {
                timer += Setting.PLAYER_SPEED;
                if (timer > 50) {
                    movePlayer(mapLong[lang]);
                    lang++;
                    timer = 0;
                }
            }
        }

        if (Setting.newClick) {
            if (numericMap[Setting.Ypos][Setting.Xpos] == 0)
                numericMap[Setting.Ypos][Setting.Xpos] = -3;
            else {
                Setting.newClick = false;
                return;
            }

            mapLong = new Logic().FindPath(getNumericMap());
            lang = 0;

            for (int i = 0; i < numericMap.length; i++) {
                for (int j = 0; j < numericMap[0].length; j++) {
                    if (numericMap[i][j] > 0) numericMap[i][j] = 0;
                    if (i == player.getY() && j == player.getX()) numericMap[i][j] = -2;
                }
            }

            numericMap[Setting.Ypos][Setting.Xpos] = 0;
            Setting.newClick = false;
        }
    }

    public void render(Graphics2D g) {
        renderMap(g);
        player.render(g);

    }

    private void renderMap(Graphics2D g) {
        for (int i = 0; i < objectMap.length; i++) {
            for (int j = 0; j < objectMap[0].length; j++) {
                objectMap[i][j].render(g);
            }
        }
    }

    public int[][] getNumericMap() {
        return numericMap;
    }

    public void ClickRender(Graphics2D g) {
        g.drawRect((int) (Setting.Xpos * Setting.TILE_WIDTH), (int) (Setting.Ypos * Setting.TILE_HEIGHT), (int) Setting.TILE_WIDTH, (int) Setting.TILE_HEIGHT);
    }

    public void movePlayer(int way) {

        switch (way) {
            case 0://Вверх
                if (player.getY() != 0) {

                    numericMap[player.getY() - 1][player.getX()] = -2;
                    numericMap[player.getY()][player.getX()] = 0;

                    player.Move(way);

                }
                break;
            case 1://Вправо
                if (player.getX() != Setting.MAP_COL) {

                    numericMap[player.getY()][player.getX() + 1] = -2;
                    numericMap[player.getY()][player.getX()] = 0;

                    player.Move(way);

                }
                break;
            case 2://Вниз
                if (player.getY() != Setting.MAP_ROW) {

                    numericMap[player.getY() + 1][player.getX()] = -2;
                    numericMap[player.getY()][player.getX()] = 0;

                    player.Move(way);

                }
                break;
            case 3://Влево
                if (player.getX() != 0) {

                    numericMap[player.getY()][player.getX() - 1] = -2;
                    numericMap[player.getY()][player.getX()] = 0;

                    player.Move(way);

                }
                break;
            default: //Ничего не изменяем
        }
    }

}
